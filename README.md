 # makedagroup multi platform project


```
 ___  __ _ _ __ ___   __ _ _ __   ___ _ __ _____   _____ 
/ __|/ _` | '_ ` _ \ / _` | '_ \ / _ \ '_ ` _ \ \ / / __|
MAKEDAGROUPE MULTI-PLATFORM APPLICATIONS BY FOPS
|___/\__,_|_| |_| |_|\__,_|_| |_|\___|_| |_| |_|\_/ \___|

```
 
## About MAKEDA APPS

MULTI-PLATFORM APPLICATIONS is a web ,mobil and api application for makedagroup
  

###  Development Technologie
Laravel
Ionic
Angular
  

## Development team


  -    Pierre Yem Mback (Full-Stack developpeur): Project manager/ Api Development/Mobile developpement / Frontend contributor - +221 77 567 21 79 - yemmbackpierre@gmail.com - https://github.com/dialrock360?tab=repositories
  -    Jason Mandabrandja  (Full-Stack developpeur) : Api Development -  +221 77 481 32 22 -  jasonmandabrandja@gmail.com
  -    Mame Cheikh Toure  (Full-Stack developpeur)  :  Frontend Development - +221 76 243 02 57 -     tourecheikhmame@gmail.com 
  -    Thonny Lars NTSOUANVA  (Full-Stack developpeur) :  Frontend Development - +221 77 510 53 57 -   larsntsouanva@gmail.com



## MAKEDAPIS  Sponsors

This project is fully sponsored by Makedagroupe, please visit the his web site [WebSite page](http://makedagroup.net/).


  -    Mr. Mohamadou bobbo : mohamadou95@yahoo.fr
  -    Mame-Amy NDOUR Taylor : mantay71@gmail.com


## Contributing

Thank you for considering contributing  to developpement team . Plaese contact manager : yemmbackpierre@gmail.com

## Code of Conduct

For all problem with application contactweb master

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License
The MAKEDAPIS project is not open source project
